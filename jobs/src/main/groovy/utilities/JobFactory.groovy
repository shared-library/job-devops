package utilities

import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.jobs.WorkflowJob

class JobFactory {

    private static final String HOST = 'http://gitlab'
    private static final String PIPELINE_REPO_BRANCH = 'main'

    static WorkflowJob seedJob(DslFactory factory) {
        String nameJob = "${factory.binding.getVariable("NAMEJOB")}"
        String technology = "${factory.binding.getVariable("TECHNOLOGY")}"
        String userGit = "${factory.binding.getVariable("USERGIT")}"
        String nameRepo = "$HOST/$userGit/${nameJob}.git"

        factory.pipelineJob(nameJob) {
            description "Job Criado automaticamente"
            parameters {
                stringParam('TECHNOLOGY', "${technology}", 'Tecnologia da aplicação')
            }
            definition {
                cpsScm {
                    scm {
                        git {
                            remote {
                                url nameRepo
                                credentials('gitlab-user')
                            }
                            branch PIPELINE_REPO_BRANCH
                        }
                        scriptPath 'Jenkinsfile'
                    }
                }
            }
        }
    }
}
